package com.ignek.action;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.annotations.Component;

import com.liferay.dynamic.data.mapping.exception.NoSuchStructureException;
import com.liferay.dynamic.data.mapping.model.DDMForm;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMStructureConstants;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.model.DDMTemplateConstants;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalServiceUtil;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalServiceUtil;
import com.liferay.dynamic.data.mapping.storage.StorageType;
import com.liferay.dynamic.data.mapping.util.DDMUtil;
import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.expando.kernel.util.ExpandoBridgeFactoryUtil;
import com.liferay.journal.model.JournalArticle;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.RoleConstants;
import com.liferay.portal.kernel.service.ClassNameLocalService;
import com.liferay.portal.kernel.service.ClassNameLocalServiceUtil;
import com.liferay.portal.kernel.service.CompanyLocalServiceUtil;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.GroupLocalServiceUtil;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.service.RoleLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, 
	property = { 
		"key=application.startup.events" 
	}, 
	service = BundleActivator.class
)

public class ConfigurationCreator implements BundleActivator {

	private static final Log log = LogFactoryUtil.getLog(ConfigurationCreator.class.getClass());
	private static final String STUDENT = "Student";
	private static final String HEAD_OF_ORGANIZATION = "Head Of Organization";
	
	DDMStructureLocalService structureLocalService = DDMStructureLocalServiceUtil.getService();
	GroupLocalService groupService = GroupLocalServiceUtil.getService();
	RoleLocalService roleService = RoleLocalServiceUtil.getService();
	ClassNameLocalService classNameService = ClassNameLocalServiceUtil.getService();
	DDMTemplateLocalService templateService = DDMTemplateLocalServiceUtil.getService();
	UserLocalService userLocalService = UserLocalServiceUtil.getService();

	@Override
	public void start(BundleContext context) throws Exception {

		/** Create custom fields **/
		/** Fetch default company in Liferay **/
		Company defaultCompany = CompanyLocalServiceUtil
				.getCompanyByMx(PropsUtil.get(PropsKeys.COMPANY_DEFAULT_WEB_ID));
		long companyId = defaultCompany.getCompanyId();
		/** Fetch Administrator role id **/
		long roleId = roleService.getRole(companyId, RoleConstants.ADMINISTRATOR).getRoleId();
		long userId = userLocalService.getRoleUsers(roleId, 0, 1).get(0).getUserId();
		try {
			ExpandoBridge expandoBridge = ExpandoBridgeFactoryUtil.getExpandoBridge(companyId, Group.class.getName(),
					0l);
			if (Validator.isNotNull(expandoBridge)) {
				int inputType = 15; //For input type text
				expandoBridge.addAttribute("Name", inputType, false);
			}

		} catch (Exception e) {
			log.error("Error occurred while creating custom field");
		}

		/** Create structure in global site **/
		long groupId = groupService.getCompanyGroup(companyId).getGroupId();
		String journalArticleClassName = JournalArticle.class.getName();
		long journalArticleClassNameId = classNameService.getClassNameId(journalArticleClassName);
		String structureKey = STUDENT;
		String content = getFile("structure/student.json");
		DDMForm ddmForm = DDMUtil.getDDMForm(content);
		ServiceContext serviceContext = new ServiceContext();
		serviceContext.setScopeGroupId(groupId);
		serviceContext.setCompanyId(companyId);
		serviceContext.setUserId(userId);
		DDMStructure studentStructure = null;
		try {
			studentStructure = structureLocalService.getStructure(groupId, journalArticleClassNameId, structureKey);
		} catch (NoSuchStructureException e) {
			log.info("No Structure found Adding new one");
		}
		if (Validator.isNotNull(studentStructure)) {
			try {
				studentStructure.setDDMForm(ddmForm);
				studentStructure = structureLocalService.updateStructure(userId, studentStructure.getStructureId(),
						ddmForm, DDMUtil.getDefaultDDMFormLayout(ddmForm), serviceContext);
			} catch (Exception e) {
				log.error("Error while updating structure");
			}

		} else {
			try {
				Map<Locale, String> nameMap = new HashMap<Locale, String>();
				nameMap.put(Locale.US, STUDENT);
				Map<Locale, String> descriptionMap = new HashMap<Locale, String>();
				descriptionMap.put(Locale.US, STUDENT);
				studentStructure = structureLocalService.addStructure(userId, groupId, StringPool.BLANK,
						journalArticleClassNameId, structureKey, nameMap, descriptionMap, ddmForm,
						DDMUtil.getDefaultDDMFormLayout(ddmForm), StorageType.JSON.toString(),
						DDMStructureConstants.TYPE_DEFAULT, serviceContext);
			} catch (Exception e) {
				log.error("Error while adding new structure");
			}
		}

		/** Create template in global site **/
		String templateKey = STUDENT;
		String script = getFile("template/student.ftl");
		String structureClassName = DDMStructure.class.getName();
		long structureClassNameId = classNameService.getClassNameId(structureClassName);
		DDMTemplate studentTemplate = null;
		try {
			studentTemplate = templateService.getTemplate(groupId, structureClassNameId, templateKey);
		} catch (Exception e) {
			log.info("No template found Adding new one");
		}
		if (Validator.isNotNull(studentTemplate)) {
			try {
				studentTemplate.setScript(script);
				studentTemplate = templateService.updateDDMTemplate(studentTemplate);
			} catch (Exception e) {
				log.error("Error while updating template");
			}

		} else {
			try {
				Map<Locale, String> nameMap = new HashMap<Locale, String>();
				nameMap.put(Locale.US, STUDENT);
				Map<Locale, String> descriptionMap = new HashMap<Locale, String>();
				descriptionMap.put(Locale.US, STUDENT);
				studentTemplate = templateService.addTemplate(userId, groupId, structureClassNameId,
						studentStructure.getStructureId(), journalArticleClassNameId, templateKey, nameMap,
						descriptionMap, DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY, StringPool.BLANK, "ftl", script,
						false, false, StringPool.BLANK, null, serviceContext);
			} catch (Exception e) {
				log.error("Error while adding a new template");
			}
		}

		/** Create role **/
		try {
			Role role = roleService.fetchRole(companyId, "Head of Organization");

			Map<Locale, String> nameMap = new HashMap<>();
			nameMap.put(Locale.US, HEAD_OF_ORGANIZATION);

			Map<Locale, String> descriptionMap = new HashMap<>();
			descriptionMap.put(Locale.US, "This role is for organization admin");
			if (Validator.isNull(role)) {
				role = roleService.addRole(userId, Role.class.getName(), 0, HEAD_OF_ORGANIZATION, nameMap,
						descriptionMap, 1, null, serviceContext);
			}
		} catch (Exception e) {
			log.error("Error while adding role");
		}
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		log.info("Stop() method called");
	}

	static public String getFile(String path) throws IOException {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		InputStream inputStream = classLoader.getResourceAsStream(path);
		return new String(FileUtil.getBytes(inputStream), "UTF-8");
	}

}
