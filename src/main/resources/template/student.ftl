<div>
    <div>${FirstName.getData()}</div>
    <div>${LastName.getData()}</div>
    <div><#assign DateOfBirth_Data = getterUtil.getString(DateOfBirth.getData())>

<#if validator.isNotNull(DateOfBirth_Data)>
	<#assign DateOfBirth_DateObj = dateUtil.parseDate("yyyy-MM-dd", DateOfBirth_Data, locale)>

	${dateUtil.getDate(DateOfBirth_DateObj, "dd MMM yyyy - HH:mm:ss", locale)}
</#if></div>
    <div>${Address.getData()}</div>
</div>